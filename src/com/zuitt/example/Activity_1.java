package com.zuitt.example;

import java.util.Scanner;
public class Activity_1 {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name: ");
        String firstName = myObj.nextLine();

        System.out.println("Last Name: ");
        String lastName = myObj.nextLine();

        System.out.println("First Subject Grade: ");
        double firstSubject = myObj.nextDouble();

        System.out.println("Second Subject Grade: ");
        double secondSubject = myObj.nextDouble();

        System.out.println("Third Subject Grade: ");
        double thirdSubject = myObj.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good Day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + average);
    }
}
